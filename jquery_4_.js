console.log("carga correcta");

$(document).on("click","button#sumar", function(){
    var valorOriginal = $("#valor").val();
    if (valorOriginal >= 10){
        console.log("El numero " + valorOriginal + " es mayor de 10");}
    else{
        var valorNuevo = parseInt(valorOriginal) + 1;
        $("#valor").val(valorNuevo);
        console.log("Al número " + valorOriginal + " se ha sumado 1 = " + valorNuevo);}
})

$(document).on("click","button#restar", function(){
    var valorOriginal = $("#valor").val();
    var valorNuevo = parseInt(valorOriginal) - 1;
    if (valorOriginal <= 0){
        console.log("El numero " + valorOriginal + " es menor de 0");}
    else{
        $("#valor").val(valorNuevo);
        console.log("Al número " + valorOriginal + " se ha restado 1 = " + valorNuevo);}
})