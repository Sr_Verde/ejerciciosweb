console.log("carga correcta");

$(document).on("click", "#sumar", function () {

    if ($("#valor5").text() == 20) return;
    for (i = 1; i < 6; i++) {
        var val = $("#valor" + i);
        var num = Number(val.text());
        var resl = num + 1;
        val.text(resl);
    }
});

$(document).on("click", "#restar", function () {
    for (i = 1; i < 6; i++) {
        var val = $("#valor" + i);
        var num = Number(val.text());
        var resl = num - 1;
        if (resl == -1) {
            break;
        }
        else val.text(resl);
    }
});  
